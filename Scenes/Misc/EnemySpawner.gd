extends Node2D

onready var player = get_parent().get_node("Player")

var initialEnemyList = Array()
var respawnQueue = Array()


func _ready():
	var enemyList = get_children()
	for enemy in enemyList:
		var duplicate = enemy.duplicate()
		duplicate.translate(Vector2(100000.0, 100000.0))
		add_child(duplicate)
		initialEnemyList.append({
			"name": enemy.name,
			"position": enemy.position,
			"global_position": enemy.global_position,
			"duplicate": duplicate
		})


func _process(delta):
	var i = 0
	while i < len(respawnQueue):
		var enemyIdx = respawnQueue[i]
		var enemy = initialEnemyList[enemyIdx]
		var player_distance = (player.global_position - enemy["global_position"]).length()
		if player_distance >= 1000:
			var duplicate = enemy["duplicate"].duplicate()
			add_child(duplicate)
			enemy["duplicate"].position.x = enemy["position"].x
			enemy["duplicate"].position.y = enemy["position"].y
			enemy["duplicate"].move_and_collide(Vector2.ZERO)
			enemy["name"] = enemy["duplicate"].name
			enemy["duplicate"] = duplicate
			respawnQueue.remove(i)
		else:
			i += 1
			

func report_kill(name: String):
	for i in range(len(initialEnemyList)):
		var enemy = initialEnemyList[i]
		if enemy["name"] == name:
			respawnQueue.append(i)
			break
