extends CanvasLayer

onready var HealthBar = get_node("TopHUD/Bars/HealthBox/HealthBar")
onready var MPBar = get_node("TopHUD/Bars/MPBox/MPBar")
onready var ExpBar = get_node("BottomHUD/Bars/ExpBar")
onready var Level = get_node("BottomHUD/Bars/LevelBar/Level")
onready var BottomHUD = get_node("BottomHUD")
onready var DialogueHUD = get_node("DialogueHUD")
onready var DialogueText = get_node("DialogueHUD/Bars/DialogueBox/DialogueText")
onready var DialogueLabel = get_node("DialogueHUD/Bars/DialogueBox/DialogueLabel")
onready var GamePause = get_node("GamePause")
onready var player = get_parent().get_node("Player")
onready var QuestMenu = get_node("QuestList")
onready var QuestListText = get_node("QuestList/MarginAll/Bars/QuestPanel/Quest")

var quest_objective = []
var objective_target = []
var objective_count = []
var quest_exp_list = []
var tutorial_message = [
	"Hello adventurer! Welcome to Project Jupyter.",
	"You are required to help a very dangerous world, fighting a lot of monsters until you are capable on fighting the demon lord.",
	"You will be reborn there as a Crimson Demon. Thus your ability will only be EXPLOSION.",
	"But before that, we will train you here.",
	"This is a sandbox world where adventurers like you can train and level up as much as you can.",
	"Yes you read it right. There is no limit for your level!",
	"Before we start, here are some instructions for you",
	"You can move by using WASD.",
	"Cast your EXPLOSION by pressing enter button.",
	"To communicate with the villager you can click j.",
	"To heal yourself, you can go and stand beside the healer.",
	"Lastly, dying will decrease your level by one, so make sure to run if your health is low.",
	"Now, try it out! Level up as much as you can and get stronger!"
]


func open_dialogue(message, type):
	player.do_nothing = true
	DialogueHUD.visible = true
	DialogueText.text = message
	if type == 0:
		DialogueLabel.text = "Press j to continue"
	elif type == 1:
		DialogueLabel.text = "Press j to accept, press k to decline"


func report_kill(enemy_name: String):
	for i in range(quest_objective.size()):
		if enemy_name == quest_objective[i]:
			objective_count[i] = min(objective_count[i] + 1, objective_target[i])


func check_quest(objective: String):
	return quest_objective.find(objective) != -1


func quest_finished(objective: String):
	var idx = quest_objective.find(objective)
	return objective_count[idx] == objective_target[idx]


func finish_quest(objective: String):
	var exp_to_give = 0
	for i in range(quest_objective.size()):
		if objective == quest_objective[i]:
			exp_to_give = quest_exp_list[i]
			quest_objective.remove(i)
			objective_target.remove(i)
			objective_count.remove(i)
			quest_exp_list.remove(i)
			break
	player.give_exp(exp_to_give)


func track_quest(objective: String, target: int, exp_reward: int):
	quest_objective.append(objective)
	objective_target.append(target)
	objective_count.append(0)
	quest_exp_list.append(exp_reward)


func close_dialogue():
	DialogueHUD.visible = false
	player.do_nothing = false


func show_tutorial(phase: int):
	if phase == len(tutorial_message):
		close_dialogue()
	else:
		open_dialogue(tutorial_message[phase], 0)


func _ready():
	pass # Replace with function body.


func toggle_quest_menu():
	if QuestMenu.visible:
		QuestMenu.visible = false
	else:
		QuestMenu.visible = true
		QuestListText.text = "Quest List:"
		for i in range(quest_objective.size()):
			QuestListText.text += """\n
- Kill %s
  Progress: %s / %s
  Reward: %s exp""" % [
				quest_objective[i], objective_count[i],
				objective_target[i], quest_exp_list[i]
			]
		if quest_objective.size() == 0:
			QuestListText.text = "You have no quest right now, try talking to the villager"


func _process(delta):
	HealthBar.max_value = player.get_max_health()
	HealthBar.value = player.get_health()
	MPBar.max_value = player.get_max_mp()
	MPBar.value = player.get_mp()
	ExpBar.max_value = player.exp_limit
	ExpBar.value = player.current_exp
	Level.text = "Level %d (HP: %d, ATK: %d)" % [player.level, player.health, player.damage]
	if Input.is_action_just_pressed("ui_cancel"):
		var new_status = !GamePause.visible
		GamePause.visible = new_status
		get_tree().paused = new_status
	if Input.is_action_just_pressed("ui_quest"):
		toggle_quest_menu()
	if player.tutorial_phase < len(tutorial_message):
		if Input.is_action_just_pressed("j") || player.tutorial_phase == -1:
			player.tutorial_phase += 1
			show_tutorial(player.tutorial_phase)


func _on_ButtonResume_pressed():
	GamePause.visible = false
	get_tree().paused = false


func _on_ButtonExit_pressed():
	Global.save_game(player)
	get_tree().quit()
