extends CanvasLayer

onready var newGameLabel = get_node("MarginAll/Bars/Play/Label")


func _ready():
	var isSaved = Global.load_game()
	if isSaved:
		newGameLabel.text = "Continue"


func _on_Play_pressed():
	get_tree().change_scene("res://Scenes/Levels/Main.tscn")


func _on_Credits_pressed():
	get_tree().change_scene("res://Scenes/Levels/Credits.tscn")


func _on_Exit_pressed():
	get_tree().quit()
