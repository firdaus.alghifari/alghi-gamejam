extends KinematicBody2D

export (int) var health = 100
export (int) var speed = 250
export (int) var damage = 10
export (float) var cast_time = 1.0
export (float) var casting_time = 2.0
export (float) var cooldown_time = 10.0
export (float) var explosion_size = 50.0
export (int) var level = 1
export (int) var exp_limit = 100

onready var Explosion = get_tree().get_root().get_node("Root/Explosion")
onready var ExplosionAnim = Explosion.get_node("ExplosionAnim")
onready var ExplosionCollision = Explosion.get_node("CollisionShape2D")
onready var ExplosionSFX = Explosion.get_node("ExplosionSFX")
onready var ExplosionCasting = get_node("ExplosionCasting")
onready var HurtSFX = get_node("PlayerHitBox/HurtSFX")
onready var BloodEffect = get_node("BloodEffect")

var velocity = Vector2()
var stand_to = Vector2(0.0, 1.0)
var last_stand = Vector2(0.0, 1.0)
var current_health = health
var current_cast_time = 0.0
var current_casting_time = 0.0
var current_cooldown_time = 0.0
var no_move = false
var do_nothing = false
var animation = ""
var current_exp = 0
var tutorial_phase = -1

func serialize():
	return {
		"health": health,
		"damage": damage,
		"level": level,
		"exp_limit": exp_limit,
		"current_health": current_health,
		"current_exp": current_exp,
		"tutorial_phase": tutorial_phase
	}
	
func load_game(save_dict):
	health = save_dict["health"]
	damage = save_dict["damage"]
	level = save_dict["level"]
	exp_limit = save_dict["exp_limit"]
	current_health = save_dict["current_health"]
	current_exp = save_dict["current_exp"]
	tutorial_phase = save_dict["tutorial_phase"]

func give_exp(exp_given: int):
	current_exp += exp_given
	while (current_exp >= exp_limit):
		current_exp -= exp_limit
		exp_limit += 50 + level * 10
		level += 1
		health += 20
		current_health += 20
		damage += 2

func heal(bonus_health: int):
	current_health += bonus_health
	current_health = min(current_health, health)

func get_max_health():
	return health

func get_health():
	return current_health

func get_max_mp():
	return cooldown_time

func get_mp():
	return cooldown_time - current_cooldown_time

func get_damage():
	return damage
	
func check_player_movement():
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x += -1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y += -1
	
func update_player_animation():
	if velocity.y >= 0.5:
		animation = "walk_down"
	elif velocity.y <= -0.5:
		animation = "walk_up"
	elif velocity.x >= 0.5:
		animation = "walk_right"
	elif velocity.x <= -0.5:
		animation = "walk_left"
	elif stand_to.y >= 0.5:
		animation = "face_down"
		last_stand = Vector2(0, 1)
	elif stand_to.y <= -0.5:
		animation = "face_up"
		last_stand = Vector2(0, -1)
	elif stand_to.x >= 0.5:
		animation = "face_right"
		last_stand = Vector2(1, 0)
	elif stand_to.x <= -0.5:
		animation = "face_left"
		last_stand = Vector2(-1, 0)
	
	ExplosionCasting.translate(ExplosionCasting.transform.get_origin() * -1)
	ExplosionCasting.translate(last_stand * 80.0)

	stand_to = velocity
	velocity = velocity.normalized() * speed
	
	if animation != $AnimatedSprite.animation:
		$AnimatedSprite.play(animation)
	
func get_input(delta):
	animation = ""
	velocity = Vector2.ZERO
	
	if Input.is_action_pressed("ui_accept"):
		current_cast_time += delta
		no_move = true
		if current_cast_time >= cast_time:
			if current_cooldown_time <= 0.0:
				ExplosionCasting.visible = false
				Explosion.visible = true
				Explosion.monitorable = true
				ExplosionCollision.get_shape().radius = 0.0
				ExplosionAnim.frame = 0
				ExplosionAnim.play("explosion")
				ExplosionAnim.translate(Vector2.ZERO)
				ExplosionAnim.translate(last_stand * 70.0)
				ExplosionSFX.play()
				current_cast_time = 0.0
				current_casting_time = casting_time
				current_cooldown_time = cooldown_time
				Explosion.set_position(self.position + last_stand * 75)
			else:
				current_cast_time = 0.0
		elif not ExplosionCasting.visible && current_cooldown_time <= 0.0:
			ExplosionCasting.frame = 0
			ExplosionCasting.play("casting")
			ExplosionCasting.visible = true
	else:
		current_cast_time = 0.0
		no_move = false
		ExplosionCasting.visible = false
	
	if current_casting_time > 0.0:
		current_casting_time -= delta
		Explosion.translate(last_stand * 70.0 * delta / casting_time)
		ExplosionAnim.translate(- last_stand * 70.0 * delta / casting_time)
		ExplosionCollision.get_shape().radius += delta / casting_time * explosion_size
		no_move = false
	elif Explosion.visible:
		Explosion.monitorable = false
		Explosion.visible = false
		current_casting_time = 0.0
	
	if current_cooldown_time > 0.0:
		current_cast_time = 0.0
		current_cooldown_time -= delta
	else:
		current_cooldown_time = 0.0
	
	if not no_move:
		check_player_movement()
	update_player_animation()

func reset_all():
	velocity = Vector2()
	stand_to = Vector2(0.0, 1.0)
	last_stand = Vector2(0.0, 1.0)
	current_health = health
	current_cast_time = 0.0
	current_casting_time = 0.0
	current_cooldown_time = 0.0
	

func _ready():
	reset_all()
	var saved_game = Global.restore_data()
	if saved_game:
		load_game(saved_game)
	

func _physics_process(delta):
	if not do_nothing:
		get_input(delta)
		velocity = move_and_collide(velocity * delta)


func _on_HitBox_area_entered(area):
	if area.is_in_group("attack_player") && self.visible:
		var pushback_direction = (global_position - area.global_position).normalized()
		move_and_collide(pushback_direction * 25)
		current_health -= area.get_parent().get_damage()
		BloodEffect.position.x = randf() * 14 - 5
		BloodEffect.position.y = randf() * 14
		BloodEffect.frame = 0
		BloodEffect.play("attacked")
		BloodEffect.visible = true
		HurtSFX.play()
		if current_health <= 0:
			do_nothing = true
			hide()
			yield(get_tree().create_timer(5.0), "timeout")
			if level > 1:
				level -= 1
				exp_limit -= 50 + level * 10
				health -= 20
				current_health = min(health, current_health)
				damage -= 2
				give_exp(0)
			do_nothing = false
			self.visible = true
			self.position.x = 0.0
			self.position.y = 0.0
			reset_all()
