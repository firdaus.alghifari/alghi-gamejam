extends KinematicBody2D

export (float) var heal_cooldown = 5.0

onready var heal_time = heal_cooldown

var player = null
var playerInside = false


func _ready():
	pass


func _process(delta):
	if playerInside:
		if heal_time <= 0.0:
			player.heal(player.get_max_health() / 10 + 10)
			heal_time = heal_cooldown
		else:
			heal_time -= delta


func _on_HealArea_area_entered(area):
	if area.name == "PlayerHitBox":
		player = area.get_parent()
		playerInside = true
		heal_time = heal_cooldown


func _on_HealArea_area_exited(area):
	if area.name == "PlayerHitBox":
		player = null
		playerInside = false
		heal_time = heal_cooldown
