extends KinematicBody2D

export (String) var quest_message = "Hello. Kill 3 monsters!"
export (String) var quest_objective = "Lv 1 Monster"
export (int) var objective_count = 3
export (int) var exp_to_give = 50

var playerInside = false
var dialogue_step = 0

onready var HUD = get_parent().get_parent().get_node("HUD")

func _process(delta):
	if playerInside:
		if dialogue_step == 0 && Input.is_action_just_pressed("j"):
			if HUD.check_quest(quest_objective):
				if HUD.quest_finished(quest_objective):
					HUD.open_dialogue("Wow! You are awesome. Here is some exp orbs for you! (+%s exp)" % exp_to_give, 0)
					HUD.finish_quest(quest_objective)
					dialogue_step = 10
				else:
					HUD.open_dialogue("The weather is nice today.", 0)
					dialogue_step = 10
			else:
				HUD.open_dialogue(quest_message, 1)
				dialogue_step += 1
		elif dialogue_step == 1:
			if Input.is_action_just_pressed("j"):
				HUD.open_dialogue("Thank you! I will be waiting here.", 0)
				HUD.track_quest(quest_objective, objective_count, exp_to_give)
				dialogue_step += 1
			elif Input.is_action_just_pressed("k"):
				HUD.open_dialogue("Okay, see you later then!", 0)	
				dialogue_step += 1
		elif Input.is_action_just_pressed("j"):
			HUD.close_dialogue()
			dialogue_step = 0


func _on_CommunicateArea_area_entered(area):
	if area.name == "PlayerHitBox":
		playerInside = true
		dialogue_step = 0


func _on_CommunicateArea_area_exited(area):
	if area.name == "PlayerHitBox":
		playerInside = false
		dialogue_step = 0
