extends KinematicBody2D

export (String) var monster_name = "Lv 1 Monster"
export (int) var health = 30
export (int) var speed = 1
export (int) var damage = 25
export (int) var exp_drop = 25

var current_health = health
var velocity = Vector2.ZERO
var stand_to = Vector2(0.0, 1.0)
var enemy = self

onready var enemySpawner = get_parent()
onready var player = enemySpawner.get_parent().get_node("Player")
onready var HUD = enemySpawner.get_parent().get_node("HUD")
onready var healthBar = get_node("HealthBar")
onready var labelName = get_node("MonsterName")

func get_damage():
	return damage

func find_player():
	var player_distance = (player.global_position - enemy.global_position).length()
	if player_distance <= 500.0:
		velocity = (player.global_position - enemy.global_position).normalized()
		velocity *= speed
	else:
		velocity = Vector2.ZERO

func _ready():
	current_health = health
	healthBar.max_value = health
	healthBar.value = current_health
	labelName.text = monster_name
	#labelName.text = monster_name + " (" + self.name + ")"
	

func _physics_process(delta):
	find_player()
	move_and_collide(velocity)


func _on_HurtBox_area_entered(area):
	if area.is_in_group("attack_enemy"):
		current_health -= player.get_damage()
		healthBar.value = current_health
		if current_health <= 0:
			player.give_exp(exp_drop)
			HUD.report_kill(monster_name)
			enemySpawner.report_kill(self.name)
			queue_free()
