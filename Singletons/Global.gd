extends Node

var game_data = null

func _ready():
	pass # Replace with function body.


"""
Really simple save file implementation. Just saving some variables to a dictionary
"""
func save_game(player): 
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://savegame.save", File.WRITE, "me0ngg_!97.1")
	var save_dict = player.serialize()
	save_game.store_line(to_json(save_dict))
	save_game.close()

"""
If check_only is true it will only check for a valid save file and return true or false without
restoring any data
"""
func load_game(check_only=false):
	var save_game = File.new()
	
	if not save_game.file_exists("user://savegame.save"):
		return false
	
	save_game.open_encrypted_with_pass("user://savegame.save", File.READ, "me0ngg_!97.1")
	
	var save_dict = parse_json(save_game.get_line())
	if typeof(save_dict) != TYPE_DICTIONARY:
		return false
	if not check_only:
		game_data = save_dict
	
	save_game.close()
	return true

"""
Restores data from the JSON dictionary inside the save files
"""
func restore_data():
	return game_data
